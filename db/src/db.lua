log = {}
mydebug = true

function log.debug(...)
    if mydebug then
        local messages = ""
        for i, msg in ipairs { ... } do
            if i ~= 1 then
                messages = messages .. " "
            end
            messages = messages .. toString(msg)
        end
        print(messages)
    end
end

function myType(val)
    if toString(val) == "userdata: 0000000000000000" then
        return type(nil)
    end
    return type(val)
end

function isString(val)
    return (type(val) == "string")
end

function isBoolean(val)
    return (type(val) == "boolean")
end

function isArray(array)
    if (type(array) == "table")
    then
        for key, val in pairs(array) do
            if isString(key) then
                return false
            end
        end
        return true
    else
        return false
    end
end

function isNumber(num)
    return (type(num) == "number")
end

function isDigit(num)
    return isNumber(num) and (string.match(tostring(num), "^%d+$") ~= null)
end

function isTable(table)
    if (type(table) == "table") then
        for key, val in pairs(table) do
            if isNumber(key) then
                return false
            end
        end
        return true
    else
        return false
    end
end

function isBlank(str)
    return str == nil or str == ""
end

function regMath(str, pattern)
    return regGet(str, pattern) ~= null
end

function regGet(str, pattern)
    return string.match(str, pattern)
end

function regFind(str, pattern)
    local res = {}
    for word in string.gmatch(str, pattern) do
        table.insert(res, word)
    end
    if #res == 0 then
        local tmp = regGet(str, pattern)
        res = (tmp ~= null and { { tmp } } or { null })[1]
    end
    return res
end

function regFirstIndex(str, pattern)
    local res = { string.find(str, pattern) }
    return (#res == 0 and { nil } or { res })[1]
end

function format(str, args, props, system)
    for index, arg in pairs(args) do
        if Variable.isVariable(arg) then
            arg = Variable.getVariableValue(arg, props, system)
        end
        str = string.gsub(str, "{}", toString(arg), 1)
    end
    return str
end

function cjson()
    return require("cjson")
end

function toString(obj)
    if type(obj) == "table" then
        local cjson = cjson()
        local jsonData = cjson.encode(obj)
        return jsonData
    elseif not isString(obj) then
        return tostring(obj)
    else
        return obj
    end
end

function toTable(json)
    local cjson = cjson()
    return cjson.decode(json)
end

function clone(element)
    if type(element) == "table" then
        local table = {}
        for i, v in pairs(element) do
            table[i] = clone(v)
        end
        return table
    else
        return element
    end
end

function createEmptyTable()
    return {}
end

function tablePut(table, key, val)
    if isBlank(key) or val == nil then
        return false
    end
    table[key] = val
    return true
end

function createEmptyArr()
    return {}
end

function arrayAdd(arr, val)
    arr[#arr + 1] = val
end

function isInArray(arr, val)
    for i, v in ipairs(arr) do
        if v == val then
            return true
        end
    end
end

function arrayRemove(arr, index)
    index = index + 1
    for i = index, (#arr - 1) do
        arr[i] = arr[i + 1]
    end
    arr[#arr] = nil
end

function arrayCopy(arr, index, length)
    if not isArray(arr) then
        return null
    end
    if index == null then
        index = 0
    end
    index = index + 1
    if index < 1 then
        index = 1
    end
    if length == null then
        length = #arr - index + 1
    end
    if index > #arr then
        return createEmptyArr()
    elseif index + length - 1 > #arr then
        length = #arr - index + 1
    end
    local res = createEmptyArr()
    for i = 1, length do
        arrayAdd(res, arr[index + i - 1])
    end
    return res
end

function replace(str, pattern, target)
    target = target or ""
    return string.gsub(str, pattern, target)
end

function fileExists(path)
    local file = io.open(path, "rb")
    if file then
        file:close()
    end
    return file ~= nil
end

function avg(arr)
    if #arr == 0 then
        return -1
    end
    local sum = 0
    for i = 1, #arr do
        local num = arr[i]
        sum = sum + num
    end
    return sum / (#arr)
end

function ssaf(arr)
    local avgNum = avg(arr)
    print(avgNum)
    local resArr = {}
    for i = 1, #arr do
        local num = arr[i]
        local dev = math.abs(num - avgNum) / avgNum
        print(num, dev)
        if dev < 0.05 then
            resArr[#resArr + 1] = num
        end
    end
    return avg(resArr)
end

Variable = {}

function Variable.isVariable(variable)
    return isString(variable) and (regMath(variable, "^%$[_%a][_%w]*$$") or Variable.isArrayVariable(variable))
end

function Variable.isArrayVariable(variable)
    return regMath(variable, "^%$[_%a][_%w]*%[[1-9]*0?%]%$$")
end

function Variable.isVariableName(variableName)
    return regMath(variableName, "^[_%a][_%w]*$") or Variable.isArrayVariableName(variableName)
end

function Variable.isArrayVariableName(variableName)
    return regMath(variableName, "^[_%a][_%w]*%[[1-9]*0?%]$")
end

function Variable.getArrayName(variableName)
    return regGet(variableName, "([_%a][_%w]*)%[")
end

function Variable.getArrayIndex(variableName)
    return tonumber(regGet(variableName, "%[([1-9]*0?)%]"))
end

function Variable.subVariableName(variable)
    return string.sub(variable, 2, -2)
end

function Variable.computeVariable(variable, props, system)
    if isString(variable) and Condition.isCondition(variable, props, system) then
        return Condition.expressionExec(variable, props, system)
    else
        return Variable.getVariableValue(variable, props, system)
    end
end

function Variable.getVariableValue(variable, props, system)
    system = system or {}
    props = props or {}
    variable = clone(variable)
    if isString(variable) and Variable.isVariable(variable) then
        local variableName = Variable.subVariableName(variable)
        if Variable.isArrayVariableName(variableName) then
            local arrayName = Variable.getArrayName(variableName)
            local arrayIndex = Variable.getArrayIndex(variableName)
            local array = (system[arrayName] ~= null and { system[arrayName] } or { props[arrayName] })[1]
            return array[arrayIndex + 1]
        end
        return (system[variableName] ~= null and { system[variableName] } or { props[variableName] })[1]
    elseif isArray(variable) then
        for i, v in ipairs(variable) do
            variable[i] = Variable.computeVariable(v, props, system)
        end
    end
    return variable
end

Expression = {}

function Expression.isLeftOperator(str)
    return regGet(str, "^%(")
end

function Expression.isRightOperator(str)
    return regGet(str, "^%)")
end

function Expression.isNum(str)
    return regGet(str, "^%d+")
end

function Expression.isVariable(str)
    return regGet(str, "^%$[_%a][_%w]*%$") or regGet(str, "^%$[_%a][_%w]*%[[1-9]*0?%]%$")
end

function Expression.isOperator(str)
    return regGet(str, "^[%+%-%*%/]")
end

function Expression.isPriorityOperator(ch)
    return regMath(ch, "^[%*%/]")
end

function Expression.isLogicalOperator(str)
    local res = regGet(str, "^<=")
    if not res then
        res = regGet(str, "^>=")
    end
    if not res then
        res = regGet(str, "^==")
    end
    if not res then
        res = regGet(str, "^!=")
    end
    if not res then
        res = regGet(str, "^[<>]")
    end
    return res
end

function Expression.getLogicalOperatorIndex(str)
    local res = regFirstIndex(str, "<=")
    if not res then
        res = regFirstIndex(str, ">=")
    end
    if not res then
        res = regFirstIndex(str, "==")
    end
    if not res then
        res = regFirstIndex(str, "!=")
    end
    if not res then
        res = regFirstIndex(str, "[<>]")
    end
    return res
end

function Expression.isNumOrVariable(str)
    local res = Expression.isNum(str)
    if not res then
        res = Expression.isVariable(str)
    end
    return res
end

function Expression.subVariableName(variable)
    return string.sub(variable, 2, -2)
end

function Expression.getValue(ch, props, system)
    if Expression.isVariable(ch) then
        return Variable.getVariableValue(ch, props, system)
    end
    return tonumber(ch)
end

function Expression.stackOperate(numStack, operatorStack)
    local num2 = numStack:pop()
    local num1 = numStack:pop()
    local operator = operatorStack:pop()
    local num = Expression.operate(num1, operator, num2)
    numStack:push(num)
end

function Expression.operate(num1, operator, num2)
    if operator == "+" then
        return num1 + num2
    elseif operator == "-" then
        return num1 - num2
    elseif operator == "*" then
        return num1 * num2
    elseif operator == "/" then
        return num1 / num2
    else
        assert(false, "未知运算符")
    end
end

function Expression.compare(num1, logicalOperator, num2)
    if logicalOperator == "==" then
        return num1 == num2
    elseif logicalOperator == "!=" then
        return num1 ~= num2
    elseif logicalOperator == ">=" then
        return num1 >= num2
    elseif logicalOperator == "<=" then
        return num1 <= num2
    elseif logicalOperator == ">" then
        return num1 > num2
    elseif logicalOperator == "<" then
        return num1 < num2
    else
        assert(false, "未知的逻辑运算符")
    end
end

function Expression.leftTrim(str, ch)
    if not isBlank(ch) then
        local len = string.len(ch)
        return string.sub(str, len + 1)
    end
end

function Expression.logicalExpressionCheck(str, props, system)
    str = replace(str, " ", "")
    local ch
    local stack = Stack:new()
    local leftToRightOperator = { ["("] = ")" }
    local count = 0

    -- 重新匹配
    :: IS_EXPRESSION_CONTINUE ::
    -- 检验所有的左符号
    repeat
        ch = Expression.isLeftOperator(str)
        if ch then
            log.debug("捕获：", ch)
            str = Expression.leftTrim(str, ch)
            stack:push(ch)
            log.debug("截取后：", str)
        end
    until ch == nil

    -- 只能是数值或变量
    ch = Expression.isNumOrVariable(str)
    if ch then
        if Expression.getValue(ch, props, system) == nil then
            return false
        end

        log.debug("捕获：", ch)
        str = Expression.leftTrim(str, ch)
        log.debug("截取后：", str)
    else
        return false
    end

    -- 只能是右符号、运算符、逻辑运算符
    repeat
        ch = Expression.isRightOperator(str)
        if ch then
            log.debug("捕获：", ch)
            str = Expression.leftTrim(str, ch)
            log.debug("截取后：", str)
            local left = stack:pop(ch)
            if leftToRightOperator[left] ~= ch then
                return false
            end
        end
    until ch == nil

    ch = Expression.isOperator(str)
    if ch then
        log.debug("捕获：", ch)
        str = Expression.leftTrim(str, ch)
        log.debug("截取后：", str)
        -- 跳转回开头
        goto IS_EXPRESSION_CONTINUE
    end

    ch = Expression.isLogicalOperator(str)
    if ch then
        log.debug("捕获：", ch)
        count = count + 1
        str = Expression.leftTrim(str, ch)
        log.debug("截取后：", str)
        -- 跳转回开头
        goto IS_EXPRESSION_CONTINUE
    end

    if isBlank(str) and count == 1 and stack:count() == 0 then
        return true
    else
        return false
    end
    return true
end

function Expression.logicalExpressionExec(str, props, system)
    assert(Expression.logicalExpressionCheck(str, props, system), "表达式不正确")
    local t = Expression.getLogicalOperatorIndex(str)
    local sub1 = string.sub(str, 1, t[1] - 1)
    local sub2 = string.sub(str, t[2] + 1)
    local operator = string.sub(str, t[1], t[2])
    local num1 = Expression.calcExpressionExec(sub1, props, system)
    local num2 = Expression.calcExpressionExec(sub2, props, system)
    return Expression.compare(num1, operator, num2)
end

function Expression.calcExpressionExec(str, props, system)
    str = replace(str, " ", "")
    local operatorStack = Stack:new()
    local numStack = Stack:new()
    local rightToLeftOperator = { [")"] = "(" }
    local ch

    -- 重新匹配
    :: EXPRESSION_EXEC_CONTINUE ::
    -- 检验所有的左符号
    repeat
        ch = Expression.isLeftOperator(str)
        if ch then
            str = Expression.leftTrim(str, ch)
            operatorStack:push(ch)
            log.debug("左符号 operator:", operatorStack:list())
            log.debug("左符号 num:", numStack:list())
            numStack:list()
        end
    until ch == nil

    -- 只能是数值或变量
    ch = Expression.isNumOrVariable(str)
    if ch then
        str = Expression.leftTrim(str, ch)
        local num = Expression.getValue(ch, props, system)
        if num == nil then
            return nil
        end
        numStack:push(num)
        log.debug("数值或变量 operator:", operatorStack:list())
        log.debug("数值或变量 num:", numStack:list())
    else
        log.debug("数值或变量 错误的值", str)
        return nil
    end

    -- 右符号
    repeat
        ch = Expression.isRightOperator(str)
        if ch then
            str = Expression.leftTrim(str, ch)
            local right = ch
            log.debug("右符号1 operator:", operatorStack:list())
            log.debug("右符号1 num:", numStack:list())
            while (rightToLeftOperator[right] ~= ch) do
                Expression.stackOperate(numStack, operatorStack)
                ch = operatorStack:pop()
                log.debug("右符号2 operator:", operatorStack:list())
                log.debug("右符号2 num:", numStack:list())
            end
        end
    until ch == nil

    -- 运算符
    ch = Expression.isOperator(str)
    if ch then
        str = Expression.leftTrim(str, ch)
        if Expression.isPriorityOperator(ch) then
            while (operatorStack:isNotEmpty() and Expression.isPriorityOperator(operatorStack:top())) do
                Expression.stackOperate(numStack, operatorStack)
            end
        else
            while (operatorStack:isNotEmpty() and Expression.isOperator(operatorStack:top())) do
                Expression.stackOperate(numStack, operatorStack)
            end
        end
        operatorStack:push(ch)
        -- 跳转回开头
        goto EXPRESSION_EXEC_CONTINUE
    end

    if isBlank(str) then
        while (operatorStack:isNotEmpty()) do
            log.debug("为空时 operator:", operatorStack:list())
            log.debug("为空时 num:", numStack:list())
            Expression.stackOperate(numStack, operatorStack)
        end
        return numStack:pop()
    end
    return nil

end

Condition = {}

function Condition.isCondition(str, props, system)
    if not isString(str) then
        return false
    end
    if Condition.isImgCondition(str) or Condition.isNoImgCondition(str) then
        return true
    elseif Condition.isExpression(str) then
        if Condition.isLogicCondition(str) then
            str = replace(str, "^logic:")
            return Expression.logicalExpressionCheck(str, props, system)
        end
        return true
    end
    return false
end

function Condition.isExpression(str)
    return Condition.isLogicCondition(str) or Condition.isCalcCondition(str)
end

function Condition.isLogicCondition(str)
    return regMath(str, "^logic:")
end

function Condition.isCalcCondition(str)
    return regMath(str, "^calc:")
end

function Condition.expressionExec(str, props, system)
    local ch = regGet(str, "^calc:")
    if ch then
        local expression = Expression.leftTrim(str, ch)
        return Expression.calcExpressionExec(expression, props, system)
    end
    ch = regGet(str, "^logic:")
    if ch then
        local expression = Expression.leftTrim(str, ch)
        return Expression.logicalExpressionExec(expression, props, system)
    end
    return nil
end

function Condition.isImgCondition(str)
    return regMath(str, "^img:")
end

function Condition.isNoImgCondition(str)
    return regMath(str, "^no%-img:")
end

-- ['move.png', [43, 671, 110, 883], 0.9, '101010', 1000]
function Condition.toImg(str, props, system)
    str = replace(str, "^img:")
    str = replace(str, "^no%-img:")
    str = replace(str, "'", [["]])
    local cjson = require("cjson")
    local imgArray = cjson.decode(str)
    if not isArray(imgArray) then
        return nil
    end
    local img = {}

    if isString(imgArray[1]) and not isBlank(imgArray[1]) then
        img["path"] = imgArray[1]
    elseif Variable.isVariable(imgArray[1]) then
        img["path"] = Variable.getVariableValue(imgArray[1], props, system)
    else
        assert(false, "未知的地址格式")
    end

    if #imgArray > 1 and isArray(imgArray[2]) then
        img["scope"] = imgArray[2]
    elseif Variable.isVariable(imgArray[2]) then
        img["scope"] = Variable.getVariableValue(imgArray[2], props, system)
    else
        img["scope"] = { 0, 0, 0, 0 }
    end

    if #imgArray > 2 and isNumber(imgArray[3]) then
        img["like"] = imgArray[3]
    elseif Variable.isVariable(imgArray[3]) then
        img["like"] = Variable.getVariableValue(imgArray[3], props, system)
    else
        img["like"] = 0.9
    end

    if #imgArray > 3 and isString(imgArray[4]) then
        img["tint"] = imgArray[4]
    elseif Variable.isVariable(imgArray[4]) then
        img["tint"] = Variable.getVariableValue(imgArray[4], props, system)
    else
        img["tint"] = '101010'
    end

    if #imgArray > 4 and isNumber(imgArray[5]) then
        img["wait_time"] = imgArray[5]
    elseif Variable.isVariable(imgArray[5]) then
        img["wait_time"] = Variable.getVariableValue(imgArray[5], props, system)
    else
        img["wait_time"] = 0
    end
    return img
end

function Condition.toImgPath(str)
    local path = replace(str, "^img:")
    if Condition.isAbsolutePath(path) then
        return path
    else
        return "Attachment:" .. path
    end
end

function Condition.isAbsolutePath(str)
    return regMath(str, "^/")
end

Base64 = {}

function Base64.encodeBase64(source_str)
    local b64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    local s64 = ''
    local str = source_str
    while #str > 0 do
        local bytes_num = 0
        local buf = 0
        for _ = 1, 3 do
            buf = (buf * 256)
            if #str > 0 then
                buf = buf + string.byte(str, 1, 1)
                str = string.sub(str, 2)
                bytes_num = bytes_num + 1
            end
        end
        for _ = 1, (bytes_num + 1) do
            local b64char = math.fmod(math.floor(buf / 262144), 64) + 1
            s64 = s64 .. string.sub(b64chars, b64char, b64char)
            buf = buf * 64
        end
        for _ = 1, (3 - bytes_num) do
            s64 = s64 .. '='
        end
    end
    return s64
end

function Base64.decodeBase64(str64)
    local b64chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    local temp = {}
    for i = 1, 64 do
        temp[string.sub(b64chars, i, i)] = i
    end
    temp['='] = 0
    local str = ""
    for i = 1, #str64, 4 do
        if i > #str64 then
            break
        end
        local data = 0
        local str_count = 0
        for j = 0, 3 do
            local str1 = string.sub(str64, i + j, i + j)
            if not temp[str1] then
                return
            end
            if temp[str1] < 1 then
                data = data * 64
            else
                data = data * 64 + temp[str1] - 1
                str_count = str_count + 1
            end
        end
        for j = 16, 0, -8 do
            if str_count > 0 then
                str = str .. string.char(math.floor(data / math.pow(2, j)))
                data = data % math.pow(2, j)
                str_count = str_count - 1
            end
        end
    end
    local last = tonumber(string.byte(str, string.len(str), string.len(str)))
    if last == 0 then
        str = string.sub(str, 1, string.len(str) - 1)
    end
    return str
end

function osRun(command)
    os.execute(command)
end

--- 类型判断
QMPlugin.type = myType
QMPlugin.isString = isString
QMPlugin.isBoolean = isBoolean
QMPlugin.isArray = isArray
QMPlugin.isNumber = isNumber
QMPlugin.isDigit = isDigit
QMPlugin.isTable = isTable
QMPlugin.isBlank = isBlank

--- 正则匹配
QMPlugin.regMath = regMath
QMPlugin.regFind = regFind

--- 误差过滤
QMPlugin.ssaf = ssaf

--- 字符串格式化
QMPlugin.format = format

--- 类型转换
QMPlugin.toString = toString
QMPlugin.toTable = toTable
QMPlugin.clone = clone

--- 数组/表操作
QMPlugin.createEmptyTable = createEmptyTable
QMPlugin.tablePut = tablePut
QMPlugin.createEmptyArr = createEmptyArr
QMPlugin.arrayAdd = arrayAdd
QMPlugin.arrayRemove = arrayRemove
QMPlugin.isInArray = isInArray
QMPlugin.arrayCopy = arrayCopy

--- 变量处理方法
QMPlugin.isVariable = Variable.isVariable
QMPlugin.isVariableName = Variable.isVariableName
QMPlugin.subVariableName = Variable.subVariableName
QMPlugin.getVariableValue = Variable.computeVariable

--- 表达式方法
QMPlugin.logicalExpressionCheck = Expression.logicalExpressionCheck
QMPlugin.logicalExpressionExec = Expression.logicalExpressionExec
QMPlugin.calcExpressionExec = Expression.calcExpressionExec

--- 条件处理方法
QMPlugin.isCondition = Condition.isCondition
QMPlugin.isExpression = Condition.isExpression
QMPlugin.expressionExec = Condition.expressionExec
QMPlugin.isImgCondition = Condition.isImgCondition
QMPlugin.isNoImgCondition = Condition.isNoImgCondition
QMPlugin.toImgPath = Condition.toImgPath
QMPlugin.toImg = Condition.toImg

--- Base64 加密解密
QMPlugin.decodeBase64 = Base64.decodeBase64
QMPlugin.encodeBase64 = Base64.encodeBase64



-- 系统命令
QMPlugin.osRun = osRun

Stack = {}

function Stack:new()
    local t = {}
    setmetatable(t, { __index = self })
    return t
end

function Stack:push(...)
    local data = self:getData()
    local arg = { ... }
    if next(arg) then
        for i = 1, #arg do
            table.insert(data, arg[i])
        end
    end
end

function Stack:pop()
    local data = self:getData()
    local top = self:top()
    table.remove(data)
    return top
end

function Stack:top()
    local data = self:getData()
    return data[#data]
end

function Stack:list()
    local data = self:getData()
    return { table.unpack(data) }
end

function Stack:count()
    return #(self:getData())
end

function Stack:getData()
    self.data = self.data or {}
    return self.data
end

function Stack:isNotEmpty()
    return self:count() ~= 0
end

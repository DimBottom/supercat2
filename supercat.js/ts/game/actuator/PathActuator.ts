import {VariableMap} from "../../core/api/variable/VariableMap";
import {PathSignature} from "../../core/api/signature/PathSignature";
import {InputSignature} from "../../core/api/signature/InputSignature";
import {SchemeSignature} from "../../core/api/signature/SchemeSignature";

/**
 * 方案执行器
 */
export class PathActuator {

    private readonly path: PathSignature;

    constructor(path: PathSignature) {
        this.path = path;
    }

    public exec(input: VariableMap) {
        let path = this.path;
        let local = this.createLocalVariable(path.props, input);
        for (let scheme of path.schemes) {
            this.schemeExec(scheme, local);
        }
    }

    private createLocalVariable(props: Array<InputSignature>, input: VariableMap): VariableMap {
        let local = new VariableMap();
        for (let prop of props) {
            // TODO 默认值可以是变量吗？
            let value = input.getLocal(prop.name, false);
            if (value == null) {
                if (prop.default == null) {
                    throw `Exec Path "${this.path.name}" fail,
                     props "${prop.name}" does not have input or default value`;
                }
                value = prop.default;
            }
            local.setLocal(prop.name, value);
        }
        return local;
    }

    private schemeExec(scheme: SchemeSignature, local: VariableMap) {
        switch (scheme.type) {
        }
    }

}
import {VariableMap} from "./core/api/variable/VariableMap";

class EnterPoint {
    public static main() {
        console.log(123)
        console.log(VariableMap.isVariable("$a123$"));
    }
}

export {EnterPoint};

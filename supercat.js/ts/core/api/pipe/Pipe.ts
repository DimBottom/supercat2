import {PipeConfig} from "./PipeConfig";
import {SystemTimes} from "../../util/SystemTimes";

/**
 * Pipe
 *
 * 任务执行的基本单元
 */
export interface Pipe {

    process(): void;

}

export abstract class AbstractPipe implements Pipe {

    protected constructor(prev: Pipe, config: PipeConfig) {
        this.prev = prev;
        this.config = config;
    }

    private readonly prev: Pipe;

    private readonly config: PipeConfig;

    public process(): void {
        this.prev.process();
        if (this.canProcess()) {
            let config = this.config;
            let startTime = SystemTimes.currentTime();
            do {
                let count = config.processCount;
                if (count == 0) {
                    while (count == 0) {
                        this.realProcess();
                    }
                } else {
                    while (count-- <= 0) {
                        this.realProcess();
                    }
                }
            } while (SystemTimes.currentTime() - startTime > config.time)
        }
    }

    protected abstract realProcess(): void;

    protected canProcess(): boolean {
        return true;
    }

}
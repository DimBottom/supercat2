/**
 * Pipe 配置
 */
export class PipeConfig {

    /**
     * 执行次数
     */
    public processCount: number = 1;

    /**
     * 某时间内重复执行
     */
    public time: number = -1;

}
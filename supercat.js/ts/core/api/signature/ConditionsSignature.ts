export enum ConditionType {
    img = "img",
    no_img = "no-img",
    logical = "logical",
    calc = "calc",
    INVALID = "invalid",
}

const INVALID_FUNCTION = () => {
}

export class Condition {

    public static readonly EMPTY = new Condition(ConditionType.INVALID, INVALID_FUNCTION);
    public static readonly EMPTY_CONDITIONS = new Array<Condition>();

    private readonly _type: ConditionType;
    private readonly _func: Function;

    constructor(type: ConditionType,
                func: Function) {
        this._type = type;
        this._func = func;
    }

    get type(): ConditionType {
        return this._type;
    }

    get func(): Function {
        return this._func;
    }
}


export class ConditionsSignature {

    public static readonly EMPTY = new ConditionsSignature(Condition.EMPTY_CONDITIONS);

    protected _conditions: Array<Condition>;

    get conditions(): Array<Condition> {
        return this._conditions;
    }

    private constructor(conditions: Array<Condition>) {
        this._conditions = conditions;
    }

    public static build(conditions: Array<Condition>): ConditionsSignature {
        if (conditions == null || conditions.length == 0) {
            return ConditionsSignature.EMPTY;
        }
        return new ConditionsSignature(conditions);
    }
}
export class InputSignature {

    private readonly _name: string;
    private readonly _default: any;

    constructor(_name: string,
                _default: any) {
        this._name = _name;
        this._default = _default;
    }


    get name(): string {
        return this._name;
    }

    get default(): any {
        return this._default;
    }
}
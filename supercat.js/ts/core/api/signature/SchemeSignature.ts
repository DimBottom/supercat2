import {Condition, ConditionsSignature} from "./ConditionsSignature";


export class IfSignature {

    private _condition: ConditionsSignature;

    constructor(condition: ConditionsSignature) {
        this._condition = condition;
    }
}

export class ForSignature {

    public static readonly EMPTY = new ForSignature(1, ConditionsSignature.EMPTY);

    private readonly _cycle: number;
    private readonly _condition: ConditionsSignature;

    private constructor(cycle: number,
                        condition: ConditionsSignature) {
        this._cycle = cycle;
        this._condition = condition;
    }

    get cycle(): number {
        return this._cycle;
    }

    get condition(): ConditionsSignature {
        return this._condition;
    }

}

export class SchemeSignature {

    private readonly _type: number;
    private readonly _if: IfSignature;
    private readonly _wait: Condition;
    private readonly _for: ForSignature;
    private readonly _delay: number;

    constructor(_type: number,
                _if: IfSignature,
                _wait: Condition,
                _for: ForSignature,
                _delay: number) {
        this._type = _type;
        this._if = _if;
        this._wait = _wait;
        this._for = _for;
        this._delay = _delay;
    }

    get type(): number {
        return this._type;
    }

    get if(): IfSignature {
        return this._if;
    }

    get wait(): Condition {
        return this._wait;
    }

    get for(): ForSignature | null {
        return this._for;
    }

    get delay(): number {
        return this._delay;
    }
}
import {SchemeSignature} from "./SchemeSignature";
import {InputSignature} from "./InputSignature";


export class PathSignature {

    public static readonly EMPTY_SCHEMES: Array<SchemeSignature> = new Array<SchemeSignature>();
    public static readonly EMPTY_PROPS: Array<InputSignature> = new Array<InputSignature>();

    private readonly _type: number;
    private readonly _name: string;
    private readonly _schemes: Array<SchemeSignature>;
    private readonly _props: Array<InputSignature>;

    constructor(type: number,
                name: string,
                schemes: Array<SchemeSignature>,
                props: Array<InputSignature>) {
        this._type = type;
        this._name = name;
        this._schemes = schemes;
        this._props = props;
    }

    get type(): number {
        return this._type;
    }

    get name(): string {
        return this._name;
    }

    get schemes(): Array<SchemeSignature> {
        return this._schemes;
    }

    get props(): Array<InputSignature> {
        return this._props;
    }
}

/**
 * 特殊关键字
 */
enum KeywordEnum {
    type,
    if,
    wait,
    for,
    delay,
}

class Keyword {

    private keywords: Set<string> = new Set<string>();

    public constructor() {
        for (let keyword in KeywordEnum) {
            this.keywords.add(keyword.valueOf());
        }
    }

    public contains(keyword: string): boolean {
        return this.keywords.has(keyword);
    }

}

export const Keywords = new Keyword()
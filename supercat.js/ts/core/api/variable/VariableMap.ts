/**
 * 参数管理器
 */
import {Keywords} from "./Keyword";

export class VariableMap {

    private static system: VariableMap = new VariableMap();

    private static variableNameRe: RegExp = new RegExp(/^[A-Za-z_]\w*$/)
    private static variableRe: RegExp = new RegExp(/^\$([A-Za-z_]\w*)\$$/)

    private arguments: Map<string, object> = new Map<string, object>();

    public static build(): VariableMap {
        return new VariableMap();
    }

    private set(variableName: string, value: any): void {
        if (VariableMap.isVariableName(variableName)) {
            this.arguments.set(VariableMap.keywordCheck(variableName), value);
        } else {
            throw `Illegal variable name: "${variableName}"`;
        }
    }

    public setLocal(variableName: string, value: any) {
        this.set(variableName, value);
    }

    public setGlobal(variableName: string, value: any) {
        VariableMap.system.set(variableName, value);
    }

    private get(variable: string) {
        return this.arguments.get(VariableMap.variableName(variable));
    }

    public getValue(variable: string): any {
        let value = this.get(variable);
        if (value == null && this != VariableMap.system) {
            return VariableMap.system.get(variable);
        }
        if (value == null) {
            throw `Global/Local variable "${variable}" does not exit.`
        }
        return value;
    }

    public getLocal(variable: string, unsafe: boolean = true): any {
        let value = this.get(variable);
        if (value == null && unsafe) {
            throw `Local variable "${variable}" does not exit.`
        }
        return value;
    }

    public getGlobal(variable: string, unsafe: boolean = true): any {
        let value = VariableMap.system.get(variable);
        if (value == null && unsafe) {
            throw `Global variable "${variable}" does not exit.`
        }
        return value;
    }

    public static isVariableName(variableName: string): boolean {
        return VariableMap.variableNameRe.test(variableName);
    }

    public static isVariable(variable: string): boolean {
        return VariableMap.variableRe.test(variable);
    }

    public static variableName(variable: string): string {
        if (VariableMap.isVariableName(variable)) {
            return VariableMap.keywordCheck(variable);
        }
        let regExpExecArray = VariableMap.variableRe.exec(variable);
        if (regExpExecArray == null || regExpExecArray.length != 2) {
            throw `Illegal variable "${variable}", extract variable name fail.`;
        }
        return VariableMap.keywordCheck(regExpExecArray[1]);
    }

    private static keywordCheck(keyword: string) {
        if (Keywords.contains(keyword)) {
            throw `Illegal variable name "${keyword}" used a keyword`;
        }
        return keyword;
    }

    public static isKeyWord(keyword: string) {
        return Keywords.contains(keyword);
    }


}
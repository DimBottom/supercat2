import {SystemTimes} from "../util/SystemTimes";

/**
 * 移动器
 */
class Mover {
    public invertXY = false; // XY轴翻转
    public start = 200; // 左初始点
    public end = 1200; // 右终止点

    public constructor(invertXY: boolean = false, start: number = 200, end: number = 1200) {
        this.invertXY = invertXY;
        this.start = start;
        this.end = end;
    }

    public right(time: number, delay?: number) {
        let distance = time * 0.5;
        let args = [[0, time, [this.start, 400], [this.start + distance, 400]]];
        if (this.invertXY) {
            invertXY(args);
        }
        gestures.apply(null, args);
        SystemTimes.delay(delay);
    }

    public left(time: number, delay?: number) {
        let distance = time * 0.5;
        if (time <= 2000) {
            let args = [[0, time, [this.end, 400], [this.end - distance, 400]]];
            if (this.invertXY) {
                invertXY(args);
            }
            gestures.apply(null, args);
        } else {
            let args = [
                [0, 2000, [this.end, 500], [this.start, 500]],
                [0, time, [this.start + distance, 400], [this.start, 400]],
            ];
            if (this.invertXY) {
                invertXY(args);
            }
            gestures.apply(null, args);
        }
        if (delay && delay > 0) {
            sleep(delay);
        }
    }

    public up() {
        swipe(300, 400, 600, 400, 200);
    }

    public down() {
        swipe(600, 400, 300, 400, 200);
    }
}

function swap(arr: any, a: number, b: number) {
    let tmp = arr[a];
    arr[a] = arr[b];
    arr[b] = tmp;
}

function invertXY(args: (number | number[])[][]) {
    for (const arg of args) {
        swap(arg[2], 0, 1);
        swap(arg[3], 0, 1);
    }
}

export { Mover };

import {SystemTimes} from "../util/SystemTimes";

/**
 * 点击器
 */
export class Pointer {
    private static PRESS_DURATION: number = 10;
    private static SWIPE_ID: number = 1;

    public swipe(x1: number, y1: number, x2: number, y2: number, id: number = Pointer.SWIPE_ID) {
        swipe(x1, y1, x2, y2, id);
    }

    public click(x: number, y: number, delay: number = 0): void {
        press(x, y, Pointer.PRESS_DURATION);
        SystemTimes.delay(delay);
    }

    public clickN(x: number, y: number, delay: number = 0, count: number = 1) {
        for (let i = 0; i < count; i++) {
            this.click(x, y, delay);
        }
    }

    public clickNX(x: number, y: number, interval: number = 50, count: number = 1, delay?: number) {
        for (let i = 0; i < count; i++) {
            press(x + i * interval, y, Pointer.PRESS_DURATION);
        }
        SystemTimes.delay(delay);
    }

    public clickNY(x: number, y: number, interval: number = 50, count: number = 1, delay?: number) {
        for (let i = 0; i < count; i++) {
            press(x, y, Pointer.PRESS_DURATION);
        }
        SystemTimes.delay(delay);
    }

    public clickPoint(p: AutoJs.Point) {
        this.click(p.x, p.y, Pointer.PRESS_DURATION);
    }
}

export abstract class Point {
    public x: number;

    public y: number;

    protected constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    public static wrap(pointer: Pointer, x: number, y: number): Point;
    public static wrap(pointer: Pointer, p: AutoJs.Point | null): Point;
    public static wrap(pointer: Pointer, p: Point | null): Point;

    public static wrap(pointer: Pointer, arg1: any, arg2?: any) {
        if (typeof arg1 == "number") {
            return new ClickablePoint(pointer, arg1, arg2);
        } else if (arg1 as AutoJs.Point) {
            return new ClickablePoint(pointer, arg1.x, arg1.y);
        } else if (arg1 as Point) {
            return arg1;
        } else {
            return InvalidPoint.InvalidPoint;
        }
    }

    public relative(x: number = 0, y: number = 0): Point {
        this.x += x;
        this.y += y;
        return this;
    }

    public abstract click(delay?: number): Point;

    public abstract clickN(count: number, delay?: number): Point;

    public abstract clickNX(interval?: number, count?: number): Point;

    public abstract clickNY(interval?: number, count?: number): Point;

    public abstract isExit(): boolean;

    public ifExit(func: Function) {
        if (this.isExit()) {
            func.apply(null, this);
        }
    }

    public toAutoPoint(): AutoJs.Point {
        return {x: this.x, y: this.y};
    }

}

export class ClickablePoint extends Point {

    private readonly pointer: Pointer;

    constructor(pointer: Pointer, x: number, y: number) {
        super(x, y);
        this.pointer = pointer;
    }

    public click(delay?: number): ClickablePoint {
        this.pointer.click(this.x, this.y, delay);
        return this;
    }

    public clickN(count: number = 1, delay?: number) {
        this.pointer.clickN(this.x, this.y, count, delay);
        return this;
    }

    public clickNX(interval: number = 50, count: number = 1, delay?: number) {
        this.pointer.clickNX(this.x, this.y, interval, count, delay)
        return this;
    }

    public clickNY(interval: number = 50, count: number = 1, delay?: number): ClickablePoint {
        this.pointer.clickNY(this.x, this.y, interval, count, delay)
        return this;
    }

    isExit(): boolean {
        return true;
    }

}

export class InvalidPoint extends Point {

    public static InvalidPoint = new InvalidPoint();

    constructor() {
        super(-1, -1);
    }

    click(delay?: number): InvalidPoint {
        return this;
    }

    clickN(count: number, delay?: number): InvalidPoint {
        return this;
    }

    clickNX(interval?: number, count?: number): InvalidPoint {
        return this;
    }

    clickNY(interval?: number, count?: number): InvalidPoint {
        return this;
    }

    isExit(): boolean {
        return false;
    }

}


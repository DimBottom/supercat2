/**
 * 系统时间工具
 */
export class SystemTimes {

    static currentTime(): number {
        return new Date().getTime();
    }

    static delay(time?: number) {
        if (time != null) sleep(time);
    }

}
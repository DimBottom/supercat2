import {Point, Pointer} from "../pointer/Pointer";

type Color = AutoJs.Color

abstract class AbstractColorPicker {

    public parseAJToMultiColors(arg: string): Array<{ 0: number; 1: number; 2: Color }> {
        let siteAndColors: string[] = arg.split(",");
        let res: Array<{ 0: number, 1: number, 2: Color }> = [];
        siteAndColors.forEach((siteAndColor, index) => {
            try {
                let {0: x, 1: y, 2: color} = siteAndColor.split("|");
                let item: { 0: number, 1: number, 2: Color } = [Number.parseInt(x), Number.parseInt(y), this.parseAJtoColor(color)];
                res.push(item);
            } catch (e) {
                throw Error(`无法解析按键的颜色 '${siteAndColor}' 的 ${siteAndColor}, index: ${index}`);
            }
        })
        return res;
    }

    public parseAJtoColor(arg: string): string {
        let colors = arg.split("-");
        return `#${colors[0]}`;
    }


}

/**
 * 拾色器
 */
export class ColorPicker extends AbstractColorPicker {

    private readonly pointer: Pointer;

    constructor(pointer: Pointer) {
        super();
        this.pointer = pointer;
    }

    public findColors(img: AutoJs.Image, color: string, colors: Array<{ 0: number; 1: number; 2: Color }>) {
        return Point.wrap(this.pointer, images.findMultiColors(img, color, colors))
    }

    public getColor(img: AutoJs.Image, x: number, y: number): number {
        return images.pixel(img, x, y);
    }

}
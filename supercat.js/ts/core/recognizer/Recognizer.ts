import {InvalidPoint, Point, Pointer} from "../pointer/Pointer";

/**
 * 图像识别
 * @param option {threshold: number, region:[number, number, number, number], center: boolean}
 */
abstract class AbstractRecognizer {

    protected constructor() {
        this.onDestroy()
    }

    public findInScreen(target: AutoJs.Image, option?: object): Point {
        let screen = this.screen();
        let p = this.findInImg(screen, target, option);
        this.recycle(screen);
        return p;
    }

    public waitFindInScreen(target: AutoJs.Image, delay: number = 0, option?: object) {
        let startTime = new Date().getTime();
        let p: Point;
        while (!(p = this.findInScreen(target, option)).isExit()) {
            if (delay != 0) {
                let cur = new Date().getTime();
                if (cur - startTime > delay) {
                    return InvalidPoint.InvalidPoint;
                }
            }
        }
        return p;
    }

    public waitFindInImg(source: AutoJs.Image, target: AutoJs.Image, delay: number = 0, option?: object) {
        let startTime = new Date().getTime();
        let p: Point;
        while (!(p = this.findInImg(source, target, option)).isExit()) {
            if (delay != 0) {
                let cur = new Date().getTime();
                if (cur - startTime > delay) {
                    return InvalidPoint.InvalidPoint;
                }
            }
        }
        return p;
    }

    /*base method*/

    public abstract findInImg(source: AutoJs.Image, target: AutoJs.Image, option?: object): Point;

    public abstract read(path: string): AutoJs.Image;

    public abstract screen(): AutoJs.Image;

    public abstract recycle(...targets: AutoJs.Image[]): void;

    public abstract destroy(): void;

    private onDestroy() {
        events.on("exit", () => {
            this.destroy();
        });
        return this;
    }

}

class Recognizer extends AbstractRecognizer {

    protected cache: Array<AutoJs.Image> = [];

    protected static inited = false;

    protected readonly pointer: Pointer;

    constructor(pointer?: Pointer) {
        super();
        if (!Recognizer.inited) {
            if (!requestScreenCapture()) {
                throw new Error("未成功取得截图权限。");
            }
            Recognizer.inited = true;
        }
        this.pointer = pointer || new Pointer();
    }

    public findInImg(source: AutoJs.Image,
                     target: AutoJs.Image,
                     option?: { threshold?: number, region?: number[], center: boolean }): Point {
        let p = findImage(source, target, option);
        if (option != null && option.center && p != null) {
            p = {
                x: p.x + target.width,
                y: p.y + target.height
            }
        }
        return Point.wrap(this.pointer, p);
    }

    public screen(): AutoJs.Image {
        return captureScreen();
    }


    public read(path: string): AutoJs.Image {
        let img = images.read(path);
        if (img == null) {
            throw new Error(`${path} not found`);
        }
        this.cache.push(img);
        return img;
    }

    public destroy() {
        for (const img of this.cache) {
            this.recycle(img);
        }
        this.cache = [];
    }

    public recycle(...targets: AutoJs.Image[]) {
        for (const target of targets) {
            if (target != null && !target.isRecycled()) {
                target.recycle();
            }
        }
    }

}

export {Recognizer};

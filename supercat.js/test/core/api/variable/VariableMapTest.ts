import {describe} from "mocha";
import {expect} from "chai";
import {VariableMap} from "../../../../ts/core/api/variable/VariableMap";

describe('VariableMap Test', () => {

    const isVariableName = VariableMap.isVariableName;
    const isVariable = VariableMap.isVariable;
    const variableName = VariableMap.variableName;

    it('should be legal variable name', function () {

        expect(isVariableName("a")).eq(true)
        expect(isVariableName("a123")).eq(true)
        expect(isVariableName("_1")).eq(true)

        expect(isVariableName("1")).eq(false)
        expect(isVariableName("1_")).eq(false)
        expect(isVariableName("")).eq(false)
        expect(isVariableName(" a1")).eq(false)
        expect(isVariableName("a1 ")).eq(false)

    });

    it('should be legal variable', function () {

        expect(isVariable("$a$")).eq(true)
        expect(isVariable("$a123$")).eq(true)
        expect(isVariable("$_1$")).eq(true)

        expect(isVariable("a")).eq(false)
        expect(isVariable("a123")).eq(false)
        expect(isVariable("_1")).eq(false)

        expect(isVariable("$a")).eq(false)
        expect(isVariable("a$")).eq(false)

        expect(isVariable("$1$")).eq(false)
        expect(isVariable("$1_$")).eq(false)
        expect(isVariable("$$")).eq(false)
        expect(isVariable(" $a1$")).eq(false)
        expect(isVariable("$a1$ ")).eq(false)

    });

    it('The correct variables should be extracted', function () {

        expect(variableName("a")).eq("a")
        expect(variableName("a123")).eq("a123")
        expect(variableName("_a")).eq("_a")

        expect(variableName("$a$")).eq("a")
        expect(variableName("$a123$")).eq("a123")
        expect(variableName("$_1$")).eq("_1")

    });

    it('should not be keyword', function () {

        expect(() => variableName("type")).throw(`Illegal variable name "type" used a keyword`)

    });

    it('get variable name from illegal str should throw error', function () {

        expect(() => variableName("$a1$ ")).throw(`Illegal variable "$a1$ ", extract variable name fail.`)

    });

    it('local variable', function () {

        let local = new VariableMap();
        local.setLocal("name", "小白");

        expect(local.getLocal("name")).eq("小白");
        expect(local.getValue("name")).eq("小白");
        expect(() => local.getGlobal("name")).throw(`Global variable "name" does not exit.`);

    });

    it('global variable', function () {

        let local = new VariableMap();
        local.setGlobal("name", "小白");

        expect(() => local.getLocal("name")).throw(`Local variable "name" does not exit.`);
        expect(local.getValue("name")).eq("小白");
        expect(local.getGlobal("name")).eq("小白");

        let local2 = new VariableMap();
        expect(local2.getGlobal("name")).eq("小白");

    });

    it(`can't create variable by keyword name`, function () {
        let local = new VariableMap();

        expect(() => local.setGlobal("type", "0"))
            .throw(`Illegal variable name "type" used a keyword`)
        expect(() => local.setLocal("type", "0"))
            .throw(`Illegal variable name "type" used a keyword`)
    });

    it(`can't get variable by keyword name`, function () {
        let local = new VariableMap();

        expect(() => local.getLocal("type"))
            .throw(`Illegal variable name "type" used a keyword`)
        expect(() => local.getLocal("type"))
            .throw(`Illegal variable name "type" used a keyword`)
    });

});
package com.dimbottom.supercat2.controller;

import com.dimbottom.supercat2.common.rest.IR;
import com.dimbottom.supercat2.common.rest.R;
import com.dimbottom.supercat2.entity.PlanVo;
import com.dimbottom.supercat2.entity.Plans;
import com.dimbottom.supercat2.service.PlanService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: 战斗策略接口
 * @author: DimBottom
 * @create: 2021/12/25 16:07
 */
@RestController
@CrossOrigin
@RequestMapping("plan")
public class PlanController {

    @Resource
    private PlanService planService;


    @PostMapping
    public IR<String> createPlan(@RequestBody PlanVo planVo) {
        String uuid = planService.savePlan(planVo);
        return R.okData(uuid);
    }

    @DeleteMapping
    public void deletePlan(@RequestParam String uuid) {
        planService.removeById(uuid);
    }

    @GetMapping("{uuid}")
    public IR<String> getPlans(@PathVariable String uuid) {
        Plans plan = planService.getById(uuid);
        return R.okData(plan.getPlan());
    }


}

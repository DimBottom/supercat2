package com.dimbottom.supercat2.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author: DimBottom
 * @create: 2021/12/25 16:42
 */
@Data
@TableName("plan")
public class Plans {

    @TableId
    private String uuid;

    private String plan;

}

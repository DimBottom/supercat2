package com.dimbottom.supercat2.entity;

import lombok.Data;

/**
 * @description: 战斗策略
 * @author: DimBottom
 * @create: 2021/12/25 16:09
 */
@Data
public class Plan {

    private String name;
    private int type;
    private Action[] actions;

}

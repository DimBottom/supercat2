package com.dimbottom.supercat2.entity;

import com.alibaba.fastjson.JSON;
import lombok.Data;

import java.util.Map;

/**
 * @description: 创建 plan vo
 * @author: DimBottom
 * @create: 2021/12/25 16:09
 */
@Data
public class PlanVo {

    private String uuid;

    private boolean override = false;

    private Map<String, Plan> plans;


    public Plans toPlans() {
        Plans plans = new Plans();
        plans.setUuid(uuid);
        plans.setPlan(JSON.toJSONString(this.plans));
        return plans;
    }

}

package com.dimbottom.supercat2.common.rest;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public interface IR<T> {


    /**
     * 状态码 .
     */
    int getCode();

    /**
     * 数据载体.
     */
    T getData();

    /**
     * 提示信息.
     */
    String getMsg();

    void setMsg(String msg);

    /**
     * 错误信息
     */
    List<String> getErrors();
}

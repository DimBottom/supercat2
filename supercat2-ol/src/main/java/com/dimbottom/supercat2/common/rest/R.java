package com.dimbottom.supercat2.common.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class R<T> implements IR<T>, Serializable {

    private static final long serialVersionUID = -3336875324869755590L;
    private int code;
    private T data;
    private String msg;
    private List<String> errors;

    private R() {
        this.code = Code.OK;
    }

    public R(int code) {
        this.code = code;
    }

    @Override
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String getMsg() {
        return msg;
    }

    @Override
    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(String... error) {
        if (errors == null) {
            errors = new ArrayList<>();
        }
        errors.addAll(Arrays.asList(error));
    }

    public static IR<Void> ok() {
        return new R<>();
    }

    public static IR<Void> ok(String msg) {
        R<Void> r = new R<>();
        r.setMsg(msg);
        return r;
    }

    public static IR<Void> ok(Integer msg) {
        R<Void> r = new R<>();
        r.setCode(msg);
        return r;
    }

    public static <T> IR<T> okData(T data) {
        R<T> r = new R<>();
        r.setData(data);
        return r;
    }

    public static <T> IR<T> okData(T data, String msg) {
        R<T> r = new R<>();
        r.setData(data);
        r.setMsg(msg);
        return r;
    }

    public static <T> IR<T> build(int code, T data) {
        R<T> r = new R<>(code);
        r.setData(data);
        return r;
    }

    public static <T> IR<T> build(int code, T data, String msg, String... errors) {
        R<T> r = new R<>(code);
        r.setData(data);
        r.setMsg(msg);
        r.setErrors(errors);
        return r;
    }


    public static IR<Void> error(String... error) {
        return failure(400, error);
    }

    public static <T> IR<T> errorData(int code, T data, String... error) {
        return failureData(code, data, error);
    }


    public static IR<Void> failure(int code, String... error) {
        R<Void> r = new R<>(code);
        r.setErrors(error);
        return r;
    }

    public static <T> IR<T> failureData(int code, T data, String... errors) {
        R<T> r = new R<>(code);
        r.setData(data);
        r.setErrors(errors);
        return r;
    }
}

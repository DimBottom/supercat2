package com.dimbottom.supercat2.common.util;

/**
 * @description: TODO
 * @author: DimBottom
 * @create: 2021/12/25 17:18
 */
public class CommonUtil {

    public static <T> T merge(T o1, T o2) {
        return o2 == null ? o1 : o2;
    }

}

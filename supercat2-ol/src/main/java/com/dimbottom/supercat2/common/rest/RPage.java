package com.dimbottom.supercat2.common.rest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class RPage<T> {

    private List<T> records;
    private long current;
    private long size;
    private long pages;
    private long total;

    public RPage(Page<T> page) {
        records = page.getRecords();
        current = page.getCurrent();
        size = page.getSize();
        pages = page.getPages();
        total = page.getTotal();
    }


    public static <T> IR<RPage<T>> ok(Page<T> page) {
        RPage<T> trPage = new RPage<>(page);
        return R.okData(trPage);
    }

    public Page<T> transform() {
        Page<T> page = new Page<>();
        page.setCurrent(current);
        page.setSize(size);
        return page;
    }

}

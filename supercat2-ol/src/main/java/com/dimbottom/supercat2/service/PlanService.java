package com.dimbottom.supercat2.service;

import cn.hutool.core.collection.CollStreamUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dimbottom.supercat2.common.util.CommonUtil;
import com.dimbottom.supercat2.entity.Plan;
import com.dimbottom.supercat2.entity.PlanVo;
import com.dimbottom.supercat2.entity.Plans;
import com.dimbottom.supercat2.mapper.PlanMapper;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author: DimBottom
 * @create: 2021/12/25 16:46
 */
@Service
public class PlanService extends ServiceImpl<PlanMapper, Plans> implements IService<Plans> {


    public String savePlan(PlanVo planVo) {
        if (StrUtil.isBlank(planVo.getUuid())) {
            planVo.setUuid(IdUtil.simpleUUID());
        }
        boolean override = planVo.isOverride();
        if (override) {
            save(planVo.toPlans());
        } else {
            Plans oldPlan = getById(planVo.getUuid());
            if (oldPlan != null) {
                Map<String, Plan> oldPlans = JSON.parseObject(oldPlan.getPlan(), new TypeReference<Map<String, Plan>>() {
                });
                Map<String, Plan> merge = CollStreamUtil.merge(oldPlans, planVo.getPlans(), CommonUtil::merge);
                planVo.setPlans(merge);
            }
            saveOrUpdate(planVo.toPlans());
        }
        return planVo.getUuid();
    }

}

package com.dimbottom.supercat2.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dimbottom.supercat2.entity.Plans;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description: Plan Mapper
 * @author: DimBottom
 * @create: 2021/12/25 16:41
 */
@Mapper
public interface PlanMapper extends BaseMapper<Plans> {


}

# 关于

本文档由 DimBottom 负责维护撰写。

## 版本信息

### 最新版本

国服：<code>{{$themeConfig.data.chinaVersion}}</code>

国际服：<code>{{$themeConfig.data.internationVersion}}</code>

先行版（国服）：<code>{{$themeConfig.data.preVersion}}</code>

## 脚本相关信息

作者：DimBottom

QQ 群：583937651

联系邮箱：2673638205@qq.com


> 注：网站图标来至网络，若存在侵权行为请联系本人删除，带来不便，敬请见谅。

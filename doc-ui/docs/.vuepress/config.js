const moment = require('moment')
moment.locale('zh-cn')

const data = {
    lastVersion: '2.1.1',
    chinaVersion: '2.1.1',
    internationVersion: '2.1.1',
    preVersion: '2.1.2',
}

module.exports = {
    title: 'SuperCat',
    base: "/supercat2/",
    description: '另一个伊甸：超越时空的猫脚本文档',
    head: [
        ['link', {rel: 'icon', href: '/logo.png'}]
    ],
    themeConfig: {
        data: data,
        lastUpdated: '更新时间',
        logo: '/logo.png',
        nav: [
            {text: '主页', link: '/'},
            {text: '工具', link: '/tools/plan-generator'},
            {text: '关于', link: '/about/'},
            {text: 'QQ群：583937651', link: 'https://jq.qq.com/?_wv=1027&k=FBE6OzXQ'},
            {text: '国际服基本库', link: 'https://gitee.com/DimBottom/supercat2/tree/master/config/int/storage'},
            {text: '国服基本库', link: 'https://gitee.com/DimBottom/supercat2/tree/master/config/zh/storage'}
        ],
        sidebar: {
            '/tools/': [
                {
                    title: '工具集',
                    collapsable: false,
                    children: [
                        '/tools/plan-generator',
                    ]
                }
            ],
            '/about/': [
                {
                    title: '关于',
                    path: '/about/',
                    collapsable: false,
                    sidebarDepth: 1,
                    children: [
                        '/about/contribute',
                        '/about/donate',
                    ]
                }
            ],
            '/': [
                {
                    title: '基本介绍',
                    path: '/',
                    collapsable: false,
                    sidebarDepth: 1,
                    children: [
                        '/introduction/environment',
                        '/introduction/manual',
                        '/introduction/FAQ',
                        '/introduction/support',
                    ]
                }, {
                    title: '更新说明',
                    path: '/history/',
                    collapsable: false,
                    sidebarDepth: 1,
                }, {
                    title: '自定义教程',
                    path: '/custom/',
                    collapsable: false,
                    sidebarDepth: 1,
                    children: [
                        '/custom/path-upload',
                        '/custom/deploy',
                        '/custom/config',
                        '/custom/path-develop',
                        '/custom/plan-develop',
                    ]
                }, {
                    title: 'API 列表',
                    path: '/api/',
                    collapsable: false,
                    sidebarDepth: 1,
                    children: [
                        '/api/Path API',
                        '/api/preset-path',
                    ]
                },
            ]
        }
    },
    plugins: [
        [
            '@vuepress/last-updated',
            {
                transformer: (timestamp) => {
                    return moment(timestamp).format('LLLL')
                }
            }
        ]
    ],
    markdown: {
        extendMarkdown: md => {
            md.block.ruler.disable('paragraph');
            md.block.ruler.after('paragraph', 'm_paragraph', (state, startLine) => {
                var content, terminate, i, l, token, oldParentType,
                    nextLine = startLine + 1,
                    terminatorRules = state.md.block.ruler.getRules('paragraph'),
                    endLine = state.lineMax;

                oldParentType = state.parentType;
                state.parentType = 'paragraph';

                // jump line-by-line until empty one or EOF
                for (; nextLine < endLine && !state.isEmpty(nextLine); nextLine++) {
                    // this would be a code block normally, but after paragraph
                    // it's considered a lazy continuation regardless of what's there
                    if (state.sCount[nextLine] - state.blkIndent > 3) {
                        continue;
                    }

                    // quirk for blockquotes, this line should already be checked by that rule
                    if (state.sCount[nextLine] < 0) {
                        continue;
                    }

                    // Some tags can terminate paragraph without empty line.
                    terminate = false;
                    for (i = 0, l = terminatorRules.length; i < l; i++) {
                        if (terminatorRules[i](state, nextLine, endLine, true)) {
                            terminate = true;
                            break;
                        }
                    }
                    if (terminate) {
                        break;
                    }
                }

                content = state.getLines(startLine, nextLine, state.blkIndent, false).trim();

                state.line = nextLine;

                token = state.push('paragraph_open', 'p', 1);
                // 如果有图片，则居中
                if (/^\!\[[\w\W]*\]\([\w\W]*\)$/.test(content)) {
                    token.attrSet('style', "text-align:center;");
                }
                token.map = [startLine, state.line];

                token = state.push('inline', '', 0);
                token.content = content;
                token.map = [startLine, state.line];
                token.children = [];

                token = state.push('paragraph_close', 'p', -1);

                state.parentType = oldParentType;

                return true;
            });
        }
    },
}
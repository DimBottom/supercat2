# 建议&BUG反馈

考虑到工作日期间不容易看群消息，而且大家对现有的bug反馈渠道不太适应，所以这里就增加一个在线文档。

主要有以下功能：

- 更新预告
- 意见征求&Bug反馈

更新预告（左侧）作者会维护，请勿随意修改，可以看到操作记录的，严重违反者飞机票。

意见征求&Bug反馈请按需要的填写（采纳和反馈不要动），我会在及时反馈，如果有描述不清楚的会我会再联系反馈者的，如果实在懒得填写也没事的，群里@或私聊我都可。

PS：万人血书的话，有其他人有同感就按当前数字+1，当然，最终采纳不采纳的决定权在我手里啦，就算你们高兴填个一万我也当作没看见(●ˇ∀ˇ●)

下附地址【腾讯文档】意见征求&Bug反馈地址：

[https://docs.qq.com/sheet/DTlh4Z21QS3lEdENl?scene=EsCfg4VBFfg4pBFfg4jKDfg4cmQHo1](https://docs.qq.com/sheet/DTlh4Z21QS3lEdENl?scene=EsCfg4VBFfg4pBFfg4jKDfg4cmQHo1)
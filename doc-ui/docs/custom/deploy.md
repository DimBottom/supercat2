# 自定义方案使用

## MuMu 添加自定义方案

### 方式一：使用自定义个入库添加

1. 按视频 2 自定义个人库配置操作，完成个人库配置。

![image-20210811220223346](https://gitee.com/DimBottom/img/raw/master/image-20210811220223346.png)

2. 将 json 文件的内容（**不含最外层花括号**）复制进入 `path.FB.json` 或 `path.SY.json` 文件中去。

![image-20210811220426924](https://gitee.com/DimBottom/img/raw/master/image-20210811220426924.png)

3. 启动脚本后，点击高级界面中的“加载”按钮，即可发现新的方案。

### 方式二：不使用自定义个人库

1. 本地创建 `path.FB.json` 文件（ `path.SY.json` 也可以）。
2. 将 json 文件的内容（**自己检查是否需要添加最外层花括号**）复制进入 `path.FB.json` 或 `path.SY.json` 文件中去。
3. 复制文件到 `我的文档/MUMU 共享文件夹` 中。
4. 利用模拟器的文件管理系统写好的文件移动到 `/storage/emulated/0/com.dimbottom.supercat/storage/` 文件夹目录中，文件就在模拟器中的 `$MUMU 共享文件中`。
5. 启动脚本后，点击高级界面中的“加载”按钮，即可发现新的方案。

<div align="center">
<img style="height:500px" src="https://gitee.com/DimBottom/img/raw/master/image-20210811220927738.png"> <img style="height:500px"  src="https://gitee.com/DimBottom/img/raw/master/image-20210811221650448.png"/>
</div>

## 其他设备

需要在电脑上完成以下操作：

1. 创建 path.FB.json 文件（ path.SY.json 也可以）。
2. 将 json 文件的内容（**自己检查是否需要添加最外层花括号**）复制进入 `path.FB.json` 或 `path.SY.json` 文件中去。
3. 发送文件到设备中（QQ 什么的各种方法）。

需要在目标设备上完成的操作：

1. 使用设备自带的功能，复制 json 文件到 `/storage/emulated/0/com.dimbottom.supercat/storage/` 文件夹目录中，具体功能请自行百度。
2. 启动脚本后，点击高级界面中的“加载”按钮，即可发现新的方案。
